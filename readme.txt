
Ci sono 3 cartelle: connessione, model e services.
Nella prima c'è la classe per la connessione singleton con il db. 
In model ci sono le tre classi: Oggetto, Ordine, Utente.
Services ha, invece, i Dao.

OggettoController e OrdineController sono le classi per interfacciarsi con 
il server di Spring.

L'html non è stato ancora implementato in quanto le varie prove sono state fatte
con postman. 
