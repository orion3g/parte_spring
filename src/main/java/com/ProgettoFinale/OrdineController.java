package com.ProgettoFinale;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ProgettoFinale.model.Oggetto;
import com.ProgettoFinale.model.Ordine;
import com.ProgettoFinale.services.OggettoDao;
import com.ProgettoFinale.services.OrdineDao;

@RestController
@RequestMapping("/ordinec")
public class OrdineController {

	@GetMapping("/recuperaordini")

	public ArrayList<Ordine> RecuperaOrdini() {

		ArrayList<Ordine> elenco = new ArrayList<Ordine>();

		OrdineDao oggDao = new OrdineDao();

		try {
			elenco = oggDao.getAll();
			return elenco;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}

	}

	@PostMapping("/faiordine")
	public Ordine nuovoOrdine(@RequestBody Ordine objOgg) {

		OrdineDao oggDao = new OrdineDao();

		try {
			oggDao.insert(objOgg);
			return objOgg;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}

	}

	@PostMapping("/modificaordine")
	public Ordine ModificaOrdine(@RequestBody Ordine objOgg) {

		OrdineDao oggDao = new OrdineDao();

		try {
			oggDao.update(objOgg);
			return objOgg;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}

	}

}
