package com.ProgettoFinale.services;

import java.security.Timestamp;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.ProgettoFinale.connessione.ConnettoreDB;
import com.ProgettoFinale.model.Oggetto;
import com.ProgettoFinale.model.Ordine;
import com.ProgettoFinale.model.Utente;

public class OrdineDao implements Dao<Ordine> {

	@Override
	public Ordine getById(Integer id) throws SQLException {

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT ordineId,dataOrdine,codiceOrdine,utenteId,username\r\n"
				+ "FROM ordine JOIN utente ON ordine.utenteRef=utente.utenteId\r\n"
				+ "where ordineId=?;";

		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, id);

		ResultSet ris = ps.executeQuery();
		ris.next();
		Ordine temp = new Ordine();

		temp.setOrdineId(ris.getInt(1));
		temp.setDataOrdine(ris.getTimestamp(2).toLocalDateTime());
		temp.setCodiceOrdine(ris.getString(3));

		Utente temp2 = new Utente();

		temp2.setUtenteId(ris.getInt(4));
		temp2.setUsername(ris.getString(5));

		return temp;

	}

	@Override
	public ArrayList<Ordine> getAll() throws SQLException {
		
		ArrayList<Ordine> elenco = new ArrayList<Ordine>();
		

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT ordineId,dataOrdine,codiceOrdine,utenteId,username\r\n"
				+ "FROM ordine JOIN utente ON ordine.utenteRef=utente.utenteId\r\n";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

		ResultSet risultato = ps.executeQuery();
		while (risultato.next()) {
			Ordine temp=new Ordine();
			Utente temp2=new Utente();
			
			temp.setOrdineId(risultato.getInt(1));
			temp.setDataOrdine(risultato.getTimestamp(2).toLocalDateTime());
			temp.setCodiceOrdine(risultato.getString(3));
			temp2.setUtenteId(risultato.getInt(4));
			temp2.setUsername(risultato.getString(5));
			
			temp.setUtente(temp2);
			
			elenco.add(temp);
		}

		return elenco;
		
	}

	@Override
	public void insert(Ordine t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "INSERT INTO ordine (codiceOrdine, utenteRef) VALUES (?,?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		 
		
		ps.setString(1, t.getCodiceOrdine());
		ps.setInt(2, t.getIdU());
		
		
		//ps.setInt(2, t.getUtenteRef().getUtenteId());
		

		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();
		t.setOrdineId(risultato.getInt(1));
		
		
		for(int i=0; i< t.getListaOggetti().size(); i++) {
			Oggetto obj = t.getListaOggetti().get(i);
			insertTabellaAppoggio(obj.getOggettoId(), t.getOrdineId());
		}
		
		
	}

	@Override
	public boolean delete(Ordine t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "DELETE FROM ordine WHERE ordineId = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, t.getOrdineId());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return true;

		return false;
	}

	
	@Override
	public Ordine update(Ordine t) throws SQLException {
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "UPDATE ordine_oggetto SET oggettoRefId= ?  WHERE codiceOrdine = ?, oggettoRefId=?  ";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		
	
	    int affRows = ps.executeUpdate();
		if (affRows > 0)
			return getById(t.getOrdineId());
		
		

		return null;
	}
	
	private void insertTabellaAppoggio(Integer idOggetto, Integer idOrdine) throws SQLException{
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "INSERT INTO ordine_oggetto (oggettoRefId, ordineRefId) VALUES (?,?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, idOggetto);
		ps.setInt(2, idOrdine);
		

		ps.executeUpdate();
	}

}
