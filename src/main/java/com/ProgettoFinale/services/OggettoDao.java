package com.ProgettoFinale.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.ProgettoFinale.connessione.ConnettoreDB;
import com.ProgettoFinale.model.Oggetto;

public class OggettoDao implements Dao<Oggetto> {

	@Override
	public Oggetto getById(Integer id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT oggettoId,nomeOggetto,descrizione FROM oggetto where oggettoId= ?";

		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, id);

		ResultSet ris = ps.executeQuery();
		ris.next();
		Oggetto temp = new Oggetto();

		temp.setOggettoId(ris.getInt(1));
		temp.setNomeOggetto(ris.getString(2));
		temp.setDescrizione(ris.getString(3));

		return temp;
	}

	@Override
	public ArrayList<Oggetto> getAll() throws SQLException {

		ArrayList<Oggetto> elencoOgg = new ArrayList<Oggetto>();
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT oggettoId,nomeOggetto,descrizione FROM oggetto";

		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ResultSet ris = ps.executeQuery();
		while (ris.next()) {
			Oggetto temp = new Oggetto();
			temp.setOggettoId(ris.getInt(1));
			temp.setNomeOggetto(ris.getString(2));
			temp.setDescrizione(ris.getString(3));
			elencoOgg.add(temp);
		}
		return elencoOgg;

	}

	@Override

	public void insert(Oggetto t) throws SQLException {

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "INSERT INTO oggetto (nomeOggetto, descrizione) VALUES (?,?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getNomeOggetto());
		ps.setString(2, t.getDescrizione());

		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();

		t.setOggettoId(risultato.getInt(1));

	}

	@Override
	public boolean delete(Oggetto t) throws SQLException {

		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "DELETE FROM oggetto WHERE oggettoId = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, t.getOggettoId());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return true;

		return false;

	}

	@Override
	public Oggetto update(Oggetto t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "UPDATE oggetto SET " + "nomeOggetto = ?, " + "descrizione = ? " + "WHERE oggettoId = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		
		ps.setString(1, t.getNomeOggetto());
		ps.setString(2, t.getDescrizione());
		ps.setInt(3, t.getOggettoId());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return getById(t.getOggettoId());

		return null;

	}

}
