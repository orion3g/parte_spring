package com.ProgettoFinale.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.ProgettoFinale.model.Utente;
import com.ProgettoFinale.connessione.ConnettoreDB;
import com.ProgettoFinale.model.Oggetto;


public class UtenteDao implements Dao<Utente>{
	
	@Override
	public Utente getById(Integer id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT 	utenteId, username, pass, tipologia FROM utente WHERE utenteId=?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, id);

		ResultSet risultato = ps.executeQuery();
		risultato.next();

		Utente temp = new Utente();
		temp.setUtenteId(risultato.getInt(1));
		temp.setUsername(risultato.getString(2));
		temp.setPass(risultato.getString(3));
		temp.setTipologia(risultato.getString(4));

		return temp;
	}
	
	@Override
	public ArrayList<Utente> getAll() throws SQLException {
		ArrayList<Utente> elenco = new ArrayList<Utente>();

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT 	utenteId, username, pass, tipologia FROM utente WHERE 1=1";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

		ResultSet risultato = ps.executeQuery();
		while (risultato.next()) {
			Utente temp = new Utente();
			temp.setUtenteId(risultato.getInt(1));
			temp.setUsername(risultato.getString(2));
			temp.setPass(risultato.getString(3));
			temp.setTipologia(risultato.getString(4));
			elenco.add(temp);
		}

		return elenco;
	}
	
	@Override
	public void insert(Utente t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "INSERT INTO utente (username, pass, tipologia) VALUES (?,?,?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getUsername());
		ps.setString(2, t.getPass());
		ps.setString(3, t.getTipologia());
		

		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();
		t.setUtenteId(risultato.getInt(1));

	}
	
	@Override
	public boolean delete(Utente t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "DELETE FROM utente WHERE utenteId = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, t.getUtenteId());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return true;

		return false;
	}
	
	@Override
	public Utente update(Utente t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "UPDATE utente SET username = ?, pass = ?, tipologia = ? WHERE utenteId = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, t.getUsername());
		ps.setString(2, t.getPass());
		ps.setString(3, t.getTipologia());
		ps.setInt(4, t.getUtenteId());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return getById(t.getUtenteId());

		return null;
	}
	
	public Utente getUtenteByUsername(String username) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT 	utenteId, username, pass, tipologia FROM utente WHERE username=?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, username);

		ResultSet risultato = ps.executeQuery();
		risultato.next();

		Utente temp = new Utente();
		temp.setUtenteId(risultato.getInt(1));
		temp.setUsername(risultato.getString(2));
		temp.setPass(risultato.getString(3));
		temp.setTipologia(risultato.getString(4));

		return temp;
	}

	

}
