package com.ProgettoFinale;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ProgettoFinale.model.Oggetto;
import com.ProgettoFinale.services.OggettoDao;

@RestController
@RequestMapping("/oggetto")
public class OggettoController {

	@PostMapping("/insertoggetto")
	
	public Oggetto inserisciOggetto(@RequestBody Oggetto objOgg) {

		OggettoDao oggDao = new OggettoDao();

		try {
			oggDao.insert(objOgg);
			return objOgg;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}

	}

	@PostMapping("/updateoggetto")
	public Oggetto UpdateOggetto(@RequestBody Oggetto objOgg) {

		OggettoDao oggDao = new OggettoDao();

		try {
			oggDao.update(objOgg);
			return objOgg;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}

	}

	@GetMapping("/recuperaoggetti")

	public ArrayList<Oggetto> RecuperaOggetti() {

		ArrayList<Oggetto> elenco = new ArrayList<Oggetto>();

		OggettoDao oggDao = new OggettoDao();

		try {
			elenco = oggDao.getAll();
			return elenco;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return null;
		}

	}

}
