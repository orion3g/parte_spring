package com.ProgettoFinale.model;

import java.util.ArrayList;

public class Oggetto {
	private Integer oggettoId;
	private String nomeOggetto;
	private String descrizione;
	
	public Oggetto(String nomeOggetto, String descrizione) {

		this.nomeOggetto = nomeOggetto;
		this.descrizione = descrizione;
	}
	public Oggetto() {
		
	}
	public Integer getOggettoId() {
		return oggettoId;
	}
	public void setOggettoId(Integer oggettoId) {
		this.oggettoId = oggettoId;
	}
	public String getNomeOggetto() {
		return nomeOggetto;
	}
	public void setNomeOggetto(String nomeOggetto) {
		this.nomeOggetto = nomeOggetto;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
	

	
	
	@Override
	public String toString() {
		return "Oggetto [oggettoId=" + oggettoId + ", nomeOggetto=" + nomeOggetto + ", descrizione=" + descrizione
				+ "]";
	}
	
	
	
}
