package com.ProgettoFinale.model;

public class Utente {
	private Integer UtenteId;
	private String username;
	private String pass;
	private String tipologia;
	
	public Utente() {
		
	}
	public Utente(Integer utenteId, String username, String pass, String tipologia) {
		
		this.username = username;
		this.pass = pass;
		this.tipologia = tipologia;
		
	}
	public Integer getUtenteId() {
		return UtenteId;
	}
	public void setUtenteId(Integer utenteId) {
		UtenteId = utenteId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getTipologia() {
		return tipologia;
	}
	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}
	@Override
	public String toString() {
		return "Utente [UtenteId=" + UtenteId + ", username=" + username + ", pass=" + pass + ", tipologia=" + tipologia
				+ "]";
	}
	
	
	 
	
}
